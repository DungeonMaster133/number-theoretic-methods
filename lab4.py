from math import sqrt, floor

from lab2 import find_power_of_2


def fermat_w_sieve(n, m: list):
    if n <= 0:
        return 'n must be positive'
    if not n % 2:
        power = find_power_of_2(n)
        print(f"2 in power of {power}")
        return power
    for i in m:
        if i <= 0:
            return 'all modules must be positive'
    x = floor(sqrt(n))  # 1
    print(x)
    if x ** 2 == n:
        return x, x
    k = len(m)
    s = []
    q = []
    for i in m:
        q.append(Q(i))
    for i in range(k):  # 2.2 таблица просеивания
        t = []
        for j in range(m[i]):
            if not (j**2 - n) % m[i] or (j**2 - n) % m[i] in q[i]:
                t.append(1)
            else:
                t.append(0)
        s.append(t)
    x += 1  # 3
    r = [x % m[i] for i in range(k)]  # ri
    while True:  # 4 просеивание
        print(x, r)
        siri_flag = True
        for i in range(k):
            if s[i][r[i]] != 1:
                siri_flag = False
                break
        if siri_flag:
            z = x**2 - n
            y = floor(sqrt(z))
            if y**2 == z:
                return x + y, x - y
        x += 1  # 5
        if x > (n + 9) / 6:
            return 'n is prime'
        r = [(r[i] + 1) % m[i] for i in range(k)]


def Q(p):
    q = []
    for i in range(1, p):
        if kronecker_jacobi(i, p) == 1:
            q.append(i)
    return q


def kronecker_jacobi(a, b):
    if b == 0:  # 1
        if abs(a) == 1:
            return 1
        else:
            return 0
    if not a % 2 and not b % 2:  # 2
        return 0
    v = 0
    while not b % 2:
        v += 1
        b /= 2
    if not v % 2:
        k = 1
    else:
        k = (-1)**((a**2 - 1) / 8)
    if b < 0:
        b -= b
        if a < 0:
            k -= k
    while True:  # 3
        if a == 0:
            if b > 1:
                return 0
            if b == 1:
                return k
        v = 0
        while not a % 2:
            v += 1
            a /= 2
        if v % 2:
            k *= (-1)**((b**2 - 1) / 8)
        k *= (-1)**((a - 1) * (b - 1) / 4)  # 4
        r = abs(a)
        a = b % r  # наименьший положительный вычет
        b = r


if __name__ == '__main__':
    print(fermat_w_sieve(int(input("n:")),
                         [int(i) for i in input(
                            "modules separated by space:").split(' ')]))
# 1751
# 3 4 5
