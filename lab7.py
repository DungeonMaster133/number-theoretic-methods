from lab1 import is_prime
from lab5 import GCD

import random
import copy
from math import floor, log2


def is_power(n):
    for k in range(2, floor(log2(n))):
        if (n ** (1/k)).is_integer():
            return True
    return False


def gauss_method(A):
    mt = len(A)  # строки
    nt = len(A[0])
    dif = mt - nt
    E = []  # единичная матрица
    for i in range(mt):
        st = []
        for j in range(mt):
            if i == j:
                st.append(1)
            else:
                st.append(0)
        E.append(st)
    k = 0
    while k < nt:
        j = k
        while j < mt and A[j][k] == 0:
            j += 1
        if j > mt - 1:
            k += 1
            continue
        if j > k:
            c = A[j]
            d = E[j]
            A[j] = A[k]
            E[j] = E[k]
            A[k] = c
            E[k] = d
        i = k
        while i < mt - 1:
            i += 1
            if A[i][k] == 1:
                for j in range(nt):
                    A[i][j] = (A[i][j] + A[k][j]) % 2
                for j in range(mt):
                    E[i][j] = (E[i][j] + E[k][j]) % 2
        k += 1
    res = []
    i = 0
    while i < dif:
        res.append(E[mt - 1 - i])
        i += 1
    return res


def dixon(n, k):
    if n < 1 or is_prime(n) or is_power(n) or n % 2 == 0:
        return 'n must be composite, uneven, not a power'
    if k <= 0:
        return 'k must be positive'
    S = [2]
    i = 3
    while len(S) != k:  # 1
        if is_prime(i):
            S.append(i)
        i += 2
    t = k + 1
    i = 0
    matrix = []
    A = []  # 2
    while True:
        while i != t:  # 2
            a = random.randint(2, n)
            if a in A:
                continue
            b = (a**2) % n
            if b == 0:
                continue
            p = b
            e_powers = []
            for s in S:  # находим разложение и запоминаем вектор степеней
                e = 0
                while b % s == 0:
                    e += 1
                    b /= s
                e_powers.append(e)
            decomp_done = False  # проверяем что разложилось
            for el in e_powers:
                if el:
                    decomp_done = True
            if not decomp_done and p != 1:
                continue
            if b == 1:
                A.append(a)
                matrix.append(e_powers)
                i += 1
        V = copy.deepcopy(matrix)
        for s in range(t):  # 3
            for b in range(k):
                V[s][b] %= 2
        solutions = gauss_method(V)  # 4
        for sol in solutions:  # 5
            I = []
            for b in range(len(sol)):
                if sol[b] == 1:
                    I.append(b)
            if I:
                x = 1  # 6
                y = 1
                for i_curr in I:  # вычисляем значение x
                    x *= A[i_curr]
                x = x % n
                if x == 1:
                    continue
                for j in range(k):  # вычисляем значение y
                    power = 0
                    for i_curr in I:
                        power += matrix[i_curr][j]
                    power /= 2
                    power = floor(power)
                    y = y * (S[j]**power) % n
                if x != y and x != (-y) % n:  # 7
                    res1 = int(GCD((x - y) % n, n))
                    res2 = int(GCD(x + y, n))
                    if res1 == 1 or res1 == n:
                        continue
                    return res1, res2
        t += 1  # все решения перебрали, строим новую пару a,b


if __name__ == '__main__':
    print(dixon(int(input("n:")), int(input("factor base size:"))))
# 77, 3
