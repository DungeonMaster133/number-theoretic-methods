from math import sqrt, floor

from lab2 import find_power_of_2


def fermat(n):
    if n <= 0:
        return "n must be positive"
    if not n % 2:
        power = find_power_of_2(n)
        print(f"2 in power of {power}")
        return power
    x = floor(sqrt(n))
    if x**2 == n:
        return (x, x)
    z = 0
    while True:
        x += 1  # 2
        if x > (n + 9) / 6:
            return "n is prime"
        if not z:  # 3
            z = x**2 - n
        else:
            z += 2*x - 1
        y = floor(sqrt(z))
        if y**2 == z:
            return (x + y, x - y)  # a,b


if __name__ == '__main__':
    print(fermat(int(input("n:"))))
#  217, 51
