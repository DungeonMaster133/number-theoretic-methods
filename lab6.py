import math

from lab1 import is_prime
from lab5 import GCD


def primes_less_than(b):
    d = [2]
    i = 3
    while i <= b:
        if is_prime(i):
            d.append(i)
        i += 2
    return d


def dichotomy_alg(x, y):
    q = x
    n = 0
    while y // (2 ** n) != 0:  # bits count
        n += 1
    if y & 1:
        z = x
    else:
        z = 1
    for i in range(1, n):
        q = q ** 2
        if y & (1 << i):
            z *= q
    return z


def p1_pollard(n, b):
    if n <= 0:
        return 'n must be positive'
    if b < 2:
        return 'b must be >= 2'
    if n % 2 == 0:
        return 'n is even'
    if is_prime(n):
        return 'n is prime'
    q = primes_less_than(b)  # primes less than B
    for a in range(2, n - 1):  # проходим по всем возможным a
        d = int(GCD(a, n))
        if d > 1:
            return d
        for i in range(len(q)):
            emax = 0
            for q_current in q:
                e_current = math.floor(math.log(n) / math.log(q_current))
                if e_current > emax:
                    emax = e_current
            a = dichotomy_alg(a, q[i] ** emax) % n
            if a == 1:
                break
            d = int(GCD(a - 1, n))
            if d > 1:
                return d
    return 'failure, limit is too low'


if __name__ == '__main__':
    print(p1_pollard(int(input("n:")), int(input("limit:"))))
# 65, 4
