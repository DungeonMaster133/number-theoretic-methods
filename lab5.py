from lab1 import is_prime


def GCD(m, n):
    if m == 0 or n == 0:  # 1
        return max(m, n)
    if m == n:
        return m
    if m == 1 or n == 1:  # 2
        return 1
    if not m % 2 and not n % 2:  # 3
        return 2 * GCD(m / 2, n / 2)
    if not m % 2 and n % 2:  # 4
        return GCD(m / 2, n)
    if not n % 2 and m % 2:  # 5
        return GCD(m, n / 2)
    if m % 2 and n % 2:  # 6, 7
        return GCD(abs(m - n) / 2, min(m, n))


def f(x, n):
    return (x ** 2 + 1) % n


def p_pollard(n):
    if n <= 0:
        return 'n must be positive'
    if not n % 2:
        return 'n is even'
    if is_prime(n):
        return 'n is prime'
    a = b = 2
    while True:  # 3
        a = f(a, n)
        b = f(f(b, n), n)
        if a == b:  # 4
            return 'failure'
        d = int(GCD(abs(a - b), n))  # 5
        if d != 1:
            return d  # 7


if __name__ == '__main__':
    print(p_pollard(int(input("n:"))))
    # 85