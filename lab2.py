from math import sqrt, floor


def olway(n, d):
    if n <= 0:
        return "n must be positive"
    if not n % 2:
        power = find_power_of_2(n)
        print(f"2 in power of {power}")
        return power
    if not d % 2 or d < 2 * round(n**(1/3)) + 1:
        return "d must be uneven and >= 2 * n**(1/3) + 1"

    r1 = n % d
    r2 = n % (d - 2)
    q = 4 * (int(n / (d - 2)) - int(n / d))
    s = floor(sqrt(n))
    while True:
        d += 2
        if d > s:
            return "no divider"
        r = 2 * r1 - r2 + q
        if r < 0:
            r = r + d
            q = q + 4
        while r >= d:
            r = r - d
            q = q - 4
        if r == 0:
            return d
        else:
            r2 = r1
            r1 = r


def find_power_of_2(n):
    i = 0
    while n % 2 == 0:
        i += 1
        n /= 2
    return i


if __name__ == '__main__':
    print(olway(int(input("n:")), int(input("d:"))))
#  1457, 23
