from lab1 import is_prime
from lab5 import GCD
from lab6 import dichotomy_alg
from lab7 import is_power, gauss_method

from math import floor, sqrt
import copy


def euler_criteria(a, p):
    return dichotomy_alg(a, int((p-1) / 2)) % p


def quadtratic_sieve(n, k):
    if n < 1 or is_prime(n) or is_power(n) or n % 2 == 0:
        return 'n must be positive, composite, uneven, not a power'
    if k < 3:
        return 'k must be at least 3'
    S = [-1, 2]
    i = 3
    while len(S) != k:  # 1
        if is_prime(i) and euler_criteria(n, i) == 1:
            S.append(i)
        i += 1
    print(S)
    t = k + 1
    m = floor(sqrt(n))
    i = 0
    matrix = []
    A = []
    x = 0
    while True:
        while i != t:
            a = x + m  # 2
            b = (a ** 2) - n
            if x == 0:  # x = 0, +-1, +-2 ...
                x += 1
            elif x > 0:
                x = -x
            elif x < 0:
                x = -x + 1
            p = b
            e_powers = []
            for s in S:  # находим разложение и запонимаем вектор степеней
                if s == -1:
                    if b < 0:
                        e_powers.append(1)
                        b = b * -1
                    else:
                        e_powers.append(0)
                    continue
                e = 0
                while b % s == 0:
                    e += 1
                    b /= s
                e_powers.append(e)
            decomp_done = False  # проверяем что разложилось
            for el in e_powers:
                if el:
                    decomp_done = True
            if not decomp_done and p != 1:
                continue
            if b == 1:
                A.append(a)
                matrix.append(e_powers)
                i += 1
        V = copy.deepcopy(matrix)
        for s in range(t):  # 3
            for b in range(k):
                V[s][b] %= 2
        solutions = gauss_method(V)  # 4
        for sol in solutions:  # 5
            I = []
            for b in range(len(sol)):
                if sol[b] == 1:
                    I.append(b)
            if I:
                x = 1  # 6
                y = 1
                for i_curr in I:  # вычисляем значение x
                    x *= A[i_curr]
                if x == 1:
                    continue
                for j in range(k):  # вычисляем значение y
                    power = 0
                    for i_curr in I:
                        power += matrix[i_curr][j]
                    power /= 2
                    power = floor(power)
                    y = y * (S[j]**power)
                if x != y and x != -y:  # 7
                    if x < 0:
                        x = x % n
                    if y < 0:
                        y = y % n
                    res1 = int(GCD(abs(x - y), n))
                    res2 = int(GCD(x + y, n))
                    if res1 == 1 or res1 == n:
                        continue
                    return res1, res2
        t += 1  # все решения перебрали, строим новую пару a,b


if __name__ == '__main__':
    print(quadtratic_sieve(int(input("n:")), int(input("factor base size:"))))
# 91, 4
