def trial_division(n: int, B=False):
    if n <= 0:
        return "n must be positive"
    if B and B < 0:
        return "limit must be positive"
    dividers = find_dividers(n)
    p = list()
    k = 0
    while True:  # 2
        flag = False
        if n == 1:
            return p
        while True:  # 3
            if B and dividers[k] > B:
                p.sort()
                return p, n
            q = n // dividers[k]
            r = n - q * dividers[k]
            if r == 0:  # 4
                p.append(dividers[k])
                n = q
                flag = True
                break
            if dividers[k] < q:  # 5
                k += 1
                continue
            break
        if flag:
            continue
        if B and n > B:
            p.sort()
            return p, n
        p.append(n)  # 6
        break
    p.sort()
    return p


def find_dividers(n):
    dividers = list()
    divider = 2
    while divider <= n / 2:
        if is_prime(divider) and n % divider == 0:
            dividers.append(divider)
        divider += 1
    if not dividers:
        dividers.append(n)
    return dividers


def is_prime(n):
    if n % 2 == 0:
        return n == 2
    d = 3
    while d * d <= n and n % d != 0:
        d += 2
    return d * d > n


if __name__ == '__main__':
    print(trial_division(int(input("n:")), int(input("limit, 0 if none:"))))
    #  825
